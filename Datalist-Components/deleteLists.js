/*
FILE:		deleteLists.js
AUTHOR:		Brody Harris

SYNOPSIS:
----------
This script is designed to run in the Alfresco Javascript console.This script will search each sites dataList folder for existing Fresh Project datalists, and delete the returned lists.

The script is comprised of 2 main fucntions:
---------------------------------------------
findSites() 	- Returns an array of sites on this instance of Fresh Project
findDataLists()	- Returns an array of pmodl: type Datalists found on the specified site
list.remove();	- Remove the specified list
*/

if (space.name != "Sites"){
	throw new Error("Not running in sites folder!");	
}

//Get sites
var sites = findSites();

for (var s in sites){
	
	//Print empty line here to seaprate the search of each site in output
	print(" ");
	
	//Run through the main logic
	var site = sites[s]
	
	var lists = findDataLists(site);
	
	for (var l in lists ){
		
		var list = lists[l];
		
		//print(getDLContent(list));
		var name =list.properties.title;
		list.remove();
		utils.getNodeFromString("archive://SpacesStore/"+list.id).remove();
		print("    " + name + " deleted");
	}
}

//	FIND THE SITES IN THE FP INSTANCE
//		Returns an array of sites
function findSites(){
	
	print("Finding sites...");
	//sites from space
	var sites = space.getChildren();
	//Check to see if there are sites
	if(sites.length === 0){
		//throw critical error if no sites found
		throw new Error("No sites found!");
	}
	//Check for non sites found in sites folder
	print("Checking for non-sites in sites folder...");
	sites = sites.filter(function filterSites(site){
		if (!site.type.match("site")){
			print(site.properties.title + " is not a site, removed");
			return false;
		}
		return true;
	});
	
	//return sites
	print(sites.length + " sites found");
	return sites;	
	
}

//	FIND THE DATALISTS IN A SITE
//		Requires a Site as parameter
//		Returns an array of dataLists
function findDataLists(site){
	
	print(" Finding DataLists in site \"" + site.properties.title + "\"...");
	//get site folders
	var siteContent = site.getChildren();
	//look for a dataLists folder in each site
	for (var f in siteContent) {
		folder = siteContent[f];
		if (folder.properties.name.match("dataLists")){
			var dlFolder = folder;
			break;
		}
	}
	//make sure datalist folder was found
	if (dlFolder === undefined){
		print(" No dataLists folder found");
		return [];
	} 
	//check if datalist folder has content
	var lists = dlFolder.getChildren();
	if (lists.length === 0){
		print(" No dataLists found");
		return [];
	}
	//Check for non sites found in sites folder
	print("Checking for non-lists in lists folder...");
	lists = lists.filter(function filterSites(list){
		
		var type = list.getProperties()["{http://www.alfresco.org/model/datalist/1.0}dataListItemType"];
		var children = list.getChildren();
		
		if (!list.type.match("list")){
			print(list.properties.title + " is not a list, removed");
			return false;
		} else if ( type.toString().indexOf("pmodl") < 0 ) {
			print(list.properties.title + " is not a Fresh-Project type DataList, removed");
			return false;
		}
		return true;
	});
	
	//Return filtered results
	print(" Found " + lists.length + " valid list(s)");
	return lists;
}
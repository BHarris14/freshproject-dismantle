/*
FILE:		typeExctractor.js
CREATED:	5/26/17
LAST CHANGED:	6/6/17
AUTHOR:		Brody Harris

SYNOPSIS:	This script is designed to run in the Alfresco Javascript console. 
		This script will search each sites dataList folder for existing datalists, extract their type, 
		and output it into the console for use in the other expor script.
	
		The script is comprised of 4 main fucntions:
		findSites() 	- Returns an array of sites on this instance of Fresh Project
		findDataLists()	- Returns an array of Datalists found on the specified site
		getDLType()	- Returns a String of the type of the datalist found
*/

print("Fresh Project datalists type and where they're found:\n-----");

var sites = findSites();
var types = new Array();

for (var s in sites){
	//Run through the main logic
	var site = sites[s];
	
	var lists = findDataLists(site);
	
	for (var l in lists ){
		
		var list = lists[l];
		
		types.push(getDLType(list));
	}
}

//Remove duplicates from array
var unique = types.filter( function(type, i, self){
	return i == self.indexOf(type);	
});

//Print unique types
print("\n\nUnique types:");
print(unique);

//	FIND THE SITES IN THE FP INSTANCE
//		Returns an array of sites
function findSites(){
	
	//sites from space
	var sites = space.getChildren();
	//Check to see if there are sites
	if(sites.length === 0){
		//throw critical error if no sites found
		throw new Error("No sites found!");
	}
	//Check for non sites found in sites folder
	sites = sites.filter(function filterSites(site){
		if (!site.type.match("site")){
			return false;
		}
		return true;
	});
	
	//return sites
	return sites;	
	
}

//	FIND THE DATALISTS IN A SITE
//		Requires a Site as parameter
//		Returns an array of dataLists
function findDataLists(site){
	
	//get site folders
	var siteContent = site.getChildren();
	//look for a dataLists folder in each site
	for (var f in siteContent) {
		folder = siteContent[f];
		if (folder.properties.name.match("dataLists")){
			var dlFolder = folder;
			break;
		}
	}
	//make sure datalist folder was found
	if (dlFolder === undefined){
		return [];
	} 
	//check if datalist folder has content
	var lists = dlFolder.getChildren();
	if (lists.length === 0){
		return [];
	}
	//Check for non sites found in sites folder
	lists = lists.filter(function filterSites(list){
		if (!list.type.match("list")){
			print(list.properties.title + " is not a site, removed");
			return false;
		}
		return true;
	});
	
	print("\nList types found in site " + site.properties.title);
	
	//Return filtered results
	return lists;
}

//	GET THE TYPE OF DATALIST
//		Requires a dataList as parameter
// 		returns the datalist type
function getDLType(list){
	
	var data = list.getProperties();

	var type = data["{http://www.alfresco.org/model/datalist/1.0}dataListItemType"]
	
	if (type.toString().indexOf("pmodl") > -1){
		print(list.properties.title + " (" + type + ")");
		return type;
	}
	
	return "";
	
}
